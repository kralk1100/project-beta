from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    employee_name = models.CharField(max_length=200)
    employee_number = models.PositiveSmallIntegerField()


class Appointment(models.Model):
    vin = models.CharField(max_length=17)
    customer_name = models.CharField(max_length=200)
    dealer_sold = models.BooleanField(null=True)
    date = models.CharField(null=True, max_length=100)
    time = models.CharField(null=True, max_length=100)
    technician_id = models.PositiveSmallIntegerField(default=1)
    reason = models.CharField(max_length=200)
    finished = models.BooleanField(default=False)
