from common.json import ModelEncoder
from .models import Appointment, Technician, AutomobileVO


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "import_href",
    ]


class TechniciansListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "employee_name",
        "employee_number",
        "id",
    ]


class AppointmentsListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "customer_name",
        "dealer_sold",
        "date",
        "time",
        "technician_id",
        "reason",
        "finished",
        "id",
    ]
