from django.db import models

# models here.
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, null=True, unique=True)

class Customer(models.Model):
    name = models.CharField(max_length=75)
    address = models.CharField(max_length=250)
    phone_number = models.CharField(max_length=30, unique=True)

class SalesPerson(models.Model):
    name = models.CharField(max_length=75)
    employee_number = models.PositiveIntegerField(unique=True)

class SalesRecord(models.Model):
    sales_person = models.ForeignKey(SalesPerson, related_name="salesrecord", on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, related_name="salesrecord", on_delete=models.CASCADE)
    sale_price = models.FloatField()
    vin = models.ForeignKey(AutomobileVO, related_name="salesrecord", on_delete=models.CASCADE)
