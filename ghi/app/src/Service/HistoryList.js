import React from "react";


class HistoryList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            vin: '',
            appointments: []
        };
    }

    async componentDidMount() {
        const url = ('http://localhost:8080/api/service/');
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({appointments:data.appointments});
        }
    }

    handleChangeVIN = (event) => {
        const value = event.target.value;
        this.setState({ vin: value });
      }


    render() {
        return (
            <div className="container">
                <h1>Enter VIN</h1>
                <div className="mb-3">
                    <select onChange={this.handleChangeVIN} value={this.state.vin} required name="VIN" id="vin">
                      <option value="">Choose a VIN</option>
                      {this.state.appointments.map(vin => {
                        return (
                          <option key={vin.vin} value={vin.vin}>{vin.vin}</option>
                        )
                      })}
                    </select>
                  </div>
                <table className="table">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Customer Name</th>
                            <th>Dealer Sold</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>Finished</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.appointments.filter(appointment => appointment.vin === this.state.vin)
                        .map(appointment => {
                            return (
                                <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.customer_name}</td>
                                <td>{JSON.stringify(appointment.dealer_sold)}</td>
                                <td>{appointment.date}</td>
                                <td>{appointment.time}</td>
                                <td>{appointment.technician}</td>
                                <td>{appointment.reason}</td>
                                <td>{JSON.stringify(appointment.finished)}</td>
                                </tr>
                            );
                            })}
                    </tbody>
                </table>
            </div>
        );
    };
}

export default HistoryList;
