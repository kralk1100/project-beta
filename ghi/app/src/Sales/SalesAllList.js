import React from 'react';

class SalesAllList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sales: [],
            };
    }
    async componentDidMount() {
        const url = 'http://localhost:8090/api/sales/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          this.setState({ sales: data.sales });
        }
      }

      render() {
        return (
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Sales List</h1>
                <table className="table table-striped">
                    <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Employee number</th>
                        <th>Customer name</th>
                        <th>VIN</th>
                        <th>Sale price $</th>
                    </tr>
                    </thead>
                    <tbody>
                        {this.state.sales.map((salesperson)=> {
                            return(
                                <tr key={salesperson.id}>
                                    <td>{salesperson.sales_person.name}</td>
                                    <td>{salesperson.sales_person.employee_number}</td>
                                    <td>{salesperson.customer.name}</td>
                                    <td>{salesperson.vin.vin}</td>
                                    <td>{salesperson.sale_price}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
                </div>
            </div>
        </div>
        );
                    }
                }

export default SalesAllList;
