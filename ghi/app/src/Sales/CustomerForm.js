import React from 'react';

class CustomerForm extends React.Component {
    state = {
      name: '',
      address:'',
      phone_number:'',
      };

    async componentDidMount() {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          this.setState({ automobiles: data.automobiles });
        }
      }

      handleNameChange = (event) => {
        const value = event.target.value;
        this.setState({ name: value });
      }

      handleAddressChange = (event) => {
        const value = event.target.value;
        this.setState({ address: value });
      }

      handlePhone_numberChange = (event) => {
        const value = event.target.value;
        this.setState({ phone_number: value });
      }

      handleSubmit = async (event) => {
        event.preventDefault();
        let data = {...this.state};
        delete data.automobiles;

        const automobileUrl = 'http://localhost:8090/api/customers/'
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(automobileUrl, fetchConfig);
        if (response.ok) {
          this.setState({
            name: '',
            address: '',
            phone_number: '',
          });
        }
      }


      render() {
        return (
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Add a new potential customer</h1>
                <form onSubmit={this.handleSubmit} id="create-customer-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleAddressChange} value={this.state.address} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
                    <label htmlFor="address">Address</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handlePhone_numberChange} value={this.state.phone_number} placeholder="Phone_number" required type="text" name="phone_number" id="phone_number" className="form-control" />
                    <label htmlFor="phone_number">Phone number</label>
                  </div>
                  <button className="btn btn-primary">Add</button>
                </form>
              </div>
            </div>
          </div>
        );
      }
    }

    export default CustomerForm;
