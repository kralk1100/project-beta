function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management!
        </p>
        <p>
        <img src="https://s3-prod.autonews.com/s3fs-public/BESTPRACTICES_306139992_AR_-1_ACXXKDJZRFEL.jpg" className="block w-100" alt="Car dealership"></img>
        </p>
      </div>
    </div>
  );
}

export default MainPage;
