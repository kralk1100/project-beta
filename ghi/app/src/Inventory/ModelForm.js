import React, {useEffect, useState} from 'react';

function ModelForm() {
    const [name, setName] = useState('');
    const [picture_url, setPicture_url] = useState('');
    const [manufacturer_id, setManufacturer] = useState('');
    const [manufacturers, setManufacturers] = useState([]);

    useEffect(() => {
        const getModelData = async () => {
            const automobileResponse = await fetch(
                "http://localhost:8100/api/manufacturers/"
                );
                const modelData = await automobileResponse.json();
                setManufacturers(modelData.manufacturers);
            };
            getModelData(); }, []);

    const handleSubmit = async event => {
        event.preventDefault();
        const data = {name, picture_url, manufacturer_id};
        const automobileUrl = 'http://localhost:8100/api/models/'
        const fetchConfig = {
            method:"post", body:JSON.stringify(data), headers:{'Content-Type':'application/json'},
        };
        const response = await fetch(automobileUrl, fetchConfig);
        if(response.ok){setName(''); setPicture_url(''); setManufacturer('');
    }
};

const handleNameChange = event => {
    setName(event.target.value);
};
const handlePicture_urlChange = event => {
    setPicture_url(event.target.value);
};
const handleManufacturerChange = event => {
    setManufacturer(event.target.value);
};

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new model</h1>
            <form onSubmit={handleSubmit} id="create-model-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePicture_urlChange} value={picture_url} placeholder="Photo" required type="text" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">Photo</label>
              </div>
              <div className="form-floating mb-3">
                <select onChange={handleManufacturerChange} value={manufacturer_id} required name="manufacturer_id" id="manufacturer_id" className="form-select" >
                <option value="manufacturer_id">Manufacturer</option>
                {manufacturers.map((model) => {
                    return (
                        <option key={model.id} value={model.id}>{model.name}</option>
                    );
                })}</select>
              </div>
              <div className="form-floating mb-3">
                </div>
              <button className="btn btn-primary">Add</button>
            </form>
          </div>
        </div>
      </div>
    );
  }

export default ModelForm;
