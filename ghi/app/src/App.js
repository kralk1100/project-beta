import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import CustomerForm from './Sales/CustomerForm';
import SalesPersonForm from './Sales/SalesPersonForm';
import SalesRecordForm from './Sales/SalesRecordForm';
import SalesAllList from './Sales/SalesAllList';
import SalesPersonSalesList from './Sales/SalesPersonSalesList';
import TechForm from './Service/TechForm';
import TechsList from './Service/TechsList';
import React from 'react';
import ManufacturerForm from './Inventory/ManufacturerForm';
import ManufacturersList from './Inventory/ManufacturersList';
import ApptForm from './Service/ApptForm';
import ApptsList from './Service/ApptsList';
import HistoryList from './Service/HistoryList';
import AutomobileForm from './Inventory/AutomobileForm';
import AutomobilesList from './Inventory/AutomobilesList';
import ModelForm from './Inventory/ModelForm';
import ModelsList from './Inventory/ModelsList';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/customers/new" element={<CustomerForm />}/>
          <Route path="/salespeople/new" element={<SalesPersonForm />}/>
          <Route path="/salesrecords/new" element={<SalesRecordForm />}/>
          <Route path="/sales/list" element={<SalesAllList />}/>
          <Route path="/employeesales/list" element={<SalesPersonSalesList />}/>
          <Route path="/techs/new" element={<TechForm />}/>
          <Route path="/techs/list" element={<TechsList />}/>
          <Route path="/service/new/" element={<ApptForm />}/>
          <Route path="/service/list" element={<ApptsList />}/>
          <Route path="/service/history/" element={<HistoryList />}/>
          <Route path="/automobiles/new" element={<AutomobileForm />}/>
          <Route path="/automobiles/list" element={<AutomobilesList />}/>
          <Route path="/manufacturers/new" element={<ManufacturerForm />}/>
          <Route path="/manufacturers/list" element={<ManufacturersList />}/>
          <Route path="/models/new" element={<ModelForm />}/>
          <Route path="/models/list" element={<ModelsList />}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
